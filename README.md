# Test driving Xstate machines

There are many reasons to like the (excellent) Xstate library. My favourites are the separation of concerns (presentation is separated from logic and data) and the visual notion of it. See: https://xstate.js.org/viz/?gist=dbfa83816cb7bcd510e0195321c20fa4

## First, why would I test a machine?

Software comes with new requirements, changes and are you willing to bet you’re chances of the machine breaking because you have no tests?  But aren’t E2E automatically generated tests enough? Well they’re at a high level and these tests tend to be very fragile, any small change can break them. 
They can also be difficult to understand or to debug. Let’s say I want to reuse my machine that I now use with React in my web app, with a new React Notive app, how do my E2E tests help me now? 

Now that we know we want to test drive our machines, let’s see an example. 

## The problem

We have an application that shows a list of flights for a date. The date can be changed, loading a new list. We’d also like to handle errors when data cannot be loaded and always upon entering the screen filtering for today. 
Thinking a little bit, we'd like something like:

![Image description](machine.png)

## Test driving the solution

In classicist TDD Red Green Refactor way, we’d start on Red with our first failing test:

```
import {Machine} from 'xstate';

describe("My machine should ", ()=>{
    it("load flights for today and transition to flights_displayed when starting",()=>{
        //GIVEN
        const expected_date=new Date();
        const current_state = 'loading_spinner_displayed';
        const machine = Machine({}).withContext({date:expected_date});
        //WHEN
        const new_state = machine.transition(current_state,'done.invoke.fetch_flights');
        //THEN
        expect(new_state.value).toBe("flights_displayed");
        expect(new_state.context.date).toBe(expected_date);
    });
});

```

Doing the code will result in:

```
import {Machine} from 'xstate';

const machineConfig = {
    id:"flightsPageMachine",
    initial:'loading_spinner_displayed',
    states:{
        loading_spinner_displayed: {
            invoke: {
                src: 'fetch_flights',
                onDone: 'flights_displayed'
            }
        },
        flights_displayed:{}
    }
};

describe("My machine should ", ()=>{
    it("load flights for today and transition to flights_displayed when starting",()=>{
        //GIVEN
        const expected_date=new Date();
        const current_state = 'loading_spinner_displayed';
        const machine = Machine(machineConfig).withContext({date:expected_date});
        //WHEN
        const new_state = machine.transition(current_state,'done.invoke.fetch_flights');
        //THEN
        expect(new_state.value).toBe("flights_displayed");
        expect(new_state.context.date).toBe(expected_date);
    });
});
```

Green
Refactor? Nothig yet...

Now Red

```
import {Machine} from 'xstate';


const machineConfig = {
    id:"flightsPageMachine",
    initial:'loading_spinner_displayed',
    states:{
        loading_spinner_displayed: {
            invoke: {
                src: 'fetch_flights',
                onDone: {
                    actions: 'collect_results',
                    target: 'flights_displayed',
                }
            }
        },
        flights_displayed:{}
    }
};

describe("My machine should ", ()=>{
    it("load flights for today and transition to flights_displayed when starting",()=>{
        //GIVEN
        const expected_date=new Date();
        const current_state = 'loading_spinner_displayed';
        const machine = Machine(machineConfig).withContext({date:expected_date});
        //WHEN
        const new_state = machine.transition(current_state,'done.invoke.fetch_flights');
        //THEN
        expect(new_state.value).toBe("flights_displayed");
        expect(new_state.context.date).toBe(expected_date);
    });

     it("transition to error_modal_displayed when failing to load flights",()=>{
        //GIVEN
        const current_state = 'loading_spinner_displayed';
        const machine = Machine(machineConfig);
        //WHEN
        const new_state = machine.transition(current_state,'error.platform.fetch_flights');
        //THEN
        expect(new_state.value).toBe("error_modal_displayed");
    });
});

```

And changing the machine to:

```
const machineConfig = {
    id:"flightsPageMachine",
    initial:'loading_spinner_displayed',
    states:{
        loading_spinner_displayed: {
            invoke: {
                src: 'fetch_flights',
                onDone: {
                    actions: 'collect_results',
                    target: 'flights_displayed',
                },
                onError:'error_modal_displayed'
            }
        },
        flights_displayed:{},
        error_modal_displayed:{}
    }
};

```

Green again. 
Refactor? Hmmm, nothing yet...

By now you can guess the next Red/Green/Refactor cycle and the resulting code:

```
import {Machine} from 'xstate';

const machineConfig = {
    id:"flightsPageMachine",
    initial:'loading_spinner_displayed',
    states:{
        loading_spinner_displayed: {
            invoke: {
                src: 'fetch_flights',
                onDone: {
                    actions: 'collect_results',
                    target: 'flights_displayed',
                },
                onError:'error_modal_displayed'
            }
        },
        flights_displayed:{
            on:{
                CHOOSE_FLIGHTS_DATE:'datepicker_modal_displayed'
            }
        },
        datepicker_modal_displayed:{},
        error_modal_displayed:{}
    }
};

describe("My machine should ", ()=>{
    it("load flights for today and transition to flights_displayed when starting",()=>{
        //GIVEN
        const expected_date=new Date();
        const current_state = 'loading_spinner_displayed';
        const machine = Machine(machineConfig).withContext({date:expected_date});
        //WHEN
        const new_state = machine.transition(current_state,'done.invoke.fetch_flights');
        //THEN
        expect(new_state.value).toBe("flights_displayed");
        expect(new_state.context.date).toBe(expected_date);
    });

    it("transition to error_modal_displayed when failing to load flights",()=>{
        //GIVEN
        const current_state = 'loading_spinner_displayed';
        const machine = Machine(machineConfig);
        //WHEN
        const new_state = machine.transition(current_state,'error.platform.fetch_flights');
        //THEN
        expect(new_state.value).toBe("error_modal_displayed");
    });

    it("transition to datepicker_modal_displayed when in and ",()=>{
        //GIVEN
        const current_state = 'flights_displayed';
        const machine = Machine(machineConfig);
        //WHEN
        const new_state = machine.transition(current_state,'CHOOSE_FLIGHTS_DATE');
        //THEN
        expect(new_state.value).toBe("datepicker_modal_displayed");
    });
});

```

In the next state I'd like to also make sure that my machine also executed an action:

```
it("invoke change_flight_date action then transition to loading_spinner_displayed when in datepicker_modal_displayed and picked new date",()=>{
        //GIVEN
        const current_state = 'datepicker_modal_displayed';
        const machine = Machine(machine_config,machine_options);
        //WHEN
        const new_state = machine.transition(current_state,'CHANGE_FLIGHT_DATE');
        //THEN
        expect(new_state.context.performed?.action).toBe("change_flights_date");
        expect(new_state.value).toBe("loading_spinner_displayed");
    });

```

For this we'd have to add a bit of accomodating code, basically having some action definitions (Remember mocking/fakes/stubs etc)?

```
import {Machine, assign} from 'xstate';

const machine_config = {
    id:"flightsPageMachine",
    initial:'loading_spinner_displayed',
    context:{},
    states:{
        loading_spinner_displayed: {
            invoke: {
                src: 'fetch_flights',
                onDone: {
                    actions: 'collect_results',
                    target: 'flights_displayed',
                },
                onError:'error_modal_displayed'
            }
        },
        flights_displayed:{
            on:{
                CHOOSE_FLIGHTS_DATE:'datepicker_modal_displayed'
            }
        },
        datepicker_modal_displayed:{
            on:{
                CHANGE_FLIGHT_DATE:{
                    actions:'change_flights_date',
                    target:'loading_spinner_displayed'
                }
            }
        },
        error_modal_displayed:{}
    }
};

const action = key => assign( {
    performed: ( ctx, ev ) => ( {
        action: key,
        context: ctx,
        event: ev
    } )
} );

const machine_options = {
    services: {
        fetch_flights: action( 'fetch_flights' )
    },
    actions: {
        change_flights_date: action( 'change_flights_date' )
    }
};

describe("My machine should ", ()=>{
    it("load flights for today and transition to flights_displayed when starting",()=>{
        //GIVEN
        const expected_date=new Date();
        const current_state = 'loading_spinner_displayed';
        const machine = Machine(machine_config).withContext({date:expected_date});
        //WHEN
        const new_state = machine.transition(current_state,'done.invoke.fetch_flights');
        //THEN
        expect(new_state.value).toBe("flights_displayed");
        expect(new_state.context.date).toBe(expected_date);
    });

    it("transition to error_modal_displayed when failing to load flights",()=>{
        //GIVEN
        const current_state = 'loading_spinner_displayed';
        const machine = Machine(machine_config);
        //WHEN
        const new_state = machine.transition(current_state,'error.platform.fetch_flights');
        //THEN
        expect(new_state.value).toBe("error_modal_displayed");
    });

    it("transition to datepicker_modal_displayed when in flights_displayed and clicked to change the flights date",()=>{
        //GIVEN
        const current_state = 'flights_displayed';
        const machine = Machine(machine_config);
        //WHEN
        const new_state = machine.transition(current_state,'CHOOSE_FLIGHTS_DATE');
        //THEN
        expect(new_state.value).toBe("datepicker_modal_displayed");
    });

    it("invoke change_flight_date action then transition to loading_spinner_displayed when in datepicker_modal_displayed and picked new date",()=>{
        //GIVEN
        const current_state = 'datepicker_modal_displayed';
        const machine = Machine(machine_config,machine_options);
        //WHEN
        const new_state = machine.transition(current_state,'CHANGE_FLIGHT_DATE');
        //THEN
        expect(new_state.context.performed?.action).toBe("change_flights_date");
        expect(new_state.value).toBe("loading_spinner_displayed");
    });
});

```

We're in Green. 
Refactor? ...hmmm... since we have a finite state machine, maybe we can write the tests in a more interesting manner. Usually this is an antipattern (asserts in if or for loops) but we'll do it anyway:

```
import {Machine, assign} from 'xstate';

const test_multi_transition = (
    machine,
    fromState,
    eventTypes
) => {
    return eventTypes
        .split( /,\s?/ )
        .reduce( ( state, eventType ) => {
            if ( typeof state === 'string' && state[ 0 ] === '{' ) {
                state = JSON.parse( state );
            }
            return machine.transition( state, eventType );
        }, fromState );
};

let machine = null;

const machine_config = {
    id:"flightsPageMachine",
    initial:'loading_spinner_displayed',
    context:{},
    states:{
        loading_spinner_displayed: {
            invoke: {
                src: 'fetch_flights',
                onDone:{
                    actions:'collect_flights',
                    target:'flights_displayed'
                },
                onError: 'error_modal_displayed'
            }
        },
        flights_displayed:{
            on:{
                CHOOSE_FLIGHTS_DATE:'datepicker_modal_displayed'
            }
        },
        datepicker_modal_displayed:{
            on:{
                CHANGE_FLIGHT_DATE:{
                    actions:'change_flights_date',
                    target:'loading_spinner_displayed'
                }
            }
        },
        error_modal_displayed:{}
    }
};

const action = key => assign( {
    performed: ( ctx, ev ) => ( {
        action: key,
        context: ctx,
        event: ev
    } )
} );

const machine_options = {
    services: {
        fetch_flights: action( 'fetchAvailabilities' )
    },
    actions: {
        collect_flights: action( 'collect_flights' ),
        change_flights_date: action( 'change_flights_date' )
    }
};

beforeEach( () => {
    machine = new Machine(machine_config, machine_options);
});

describe( 'Search flights ,achine should', () => {
    const expected = {
        loading_spinner_displayed: {
            'done.invoke.fetch_flights': ['flights_displayed', 'collect_flights'],
            'error.platform.fetch_flights': 'error_modal_displayed'
        },
        flights_displayed: {
            CHOOSE_FLIGHTS_DATE: 'datepicker_modal_displayed'
        },
        datepicker_modal_displayed: {
            CHANGE_FLIGHT_DATE: ['loading_spinner_displayed', 'change_flights_date']
        },
        error_modal_displayed: {
        }
    };

    Object.keys( expected ).forEach( from_state => {
        Object.keys( expected[ from_state ] ).forEach( event_types => {
            const toStateResult = expected[ from_state ][ event_types ];
            const [toState, actionPerformed] = Array.isArray( toStateResult ) ? toStateResult : [toStateResult,];
            const performed = actionPerformed?` performing ${actionPerformed} `:'';

            it( `go from ${from_state} to ${JSON.stringify(
                toState
            )} on ${event_types}${performed}`, () => {
                const result_state = test_multi_transition( machine, from_state, event_types );

                expect( result_state.value).toBe(toState );
                expect( result_state.context.performed?.action).toBe( actionPerformed );
            } );
        } );
    } );
} );
```

Instead of making a test for each transition, we create an object with all the states and events and the expected states after transitions as well as eventual actions.

The results are fairly interesting:

```
 Search flights machine should
    ✓ go from loading_spinner_displayed to "flights_displayed" on done.invoke.fetch_flights performing collect_flights  (7ms)
    ✓ go from loading_spinner_displayed to "error_modal_displayed" on error.platform.fetch_flights (1ms)
    ✓ go from flights_displayed to "datepicker_modal_displayed" on CHOOSE_FLIGHTS_DATE (1ms)
    ✓ go from datepicker_modal_displayed to "loading_spinner_displayed" on CHANGE_FLIGHT_DATE performing change_flights_date  (1ms)
```
 Now from this we'll move to Red by adding a new line to the expected object: 


```
error_modal_displayed: {
            CLOSE:'flights_displayed'
        }
```

and to move to green one in the machine:

```
error_modal_displayed:{
               on: {
                   CLOSE: 'flights_displayed'
               }
           }
```
 resulting in: 
 
```
 Search flights machine should
    ✓ go from loading_spinner_displayed to "flights_displayed" on done.invoke.fetch_flights performing collect_flights  (7ms)
    ✓ go from loading_spinner_displayed to "error_modal_displayed" on error.platform.fetch_flights (1ms)
    ✓ go from flights_displayed to "datepicker_modal_displayed" on CHOOSE_FLIGHTS_DATE (1ms)
    ✓ go from datepicker_modal_displayed to "loading_spinner_displayed" on CHANGE_FLIGHT_DATE performing change_flights_date  (1ms)
    ✓ go from error_modal_displayed to "flights_displayed" on CLOSE
```

Sweet!!!

### Next steps

We'll stop here today, but the next step would be to test drive the actions.
After that move up and test drive a React UI, ending up finally with a fully covered application.
But that will be next time!   

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

