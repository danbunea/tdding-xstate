import {Machine, assign} from 'xstate';

const test_multi_transition = (
    machine,
    fromState,
    eventTypes
) => {
    return eventTypes
        .split( /,\s?/ )
        .reduce( ( state, eventType ) => {
            if ( typeof state === 'string' && state[ 0 ] === '{' ) {
                state = JSON.parse( state );
            }
            return machine.transition( state, eventType );
        }, fromState );
};

let machine = null;

const machine_config = {
    id:"flightsPageMachine",
    initial:'loading_spinner_displayed',
    context:{},
    states:{
        loading_spinner_displayed: {
            invoke: {
                src: 'fetch_flights',
                onDone:{
                    actions:'collect_flights',
                    target:'flights_displayed'
                },
                onError: 'error_modal_displayed'
            }
        },
        flights_displayed:{
            on:{
                CHOOSE_FLIGHTS_DATE:'datepicker_modal_displayed'
            }
        },
        datepicker_modal_displayed:{
            on:{
                CHANGE_FLIGHT_DATE:{
                    actions:'change_flights_date',
                    target:'loading_spinner_displayed'
                }
            }
        },
        error_modal_displayed:{
            on: {
                CLOSE: 'flights_displayed'
            }
        }
    }
};

const action = key => assign( {
    performed: ( ctx, ev ) => ( {
        action: key,
        context: ctx,
        event: ev
    } )
} );

const machine_options = {
    services: {
        fetch_flights: action( 'fetchAvailabilities' )
    },
    actions: {
        collect_flights: action( 'collect_flights' ),
        change_flights_date: action( 'change_flights_date' )
    }
};





beforeEach( () => {
    machine = new Machine(machine_config, machine_options);
});

describe( 'Search flights ,achine should', () => {
    const expected = {
        loading_spinner_displayed: {
            'done.invoke.fetch_flights': ['flights_displayed', 'collect_flights'],
            'error.platform.fetch_flights': 'error_modal_displayed'
        },
        flights_displayed: {
            CHOOSE_FLIGHTS_DATE: 'datepicker_modal_displayed'
        },
        datepicker_modal_displayed: {
            CHANGE_FLIGHT_DATE: ['loading_spinner_displayed', 'change_flights_date']
        },
        error_modal_displayed: {
            CLOSE:'flights_displayed'
        }
    };

    Object.keys( expected ).forEach( from_state => {
        Object.keys( expected[ from_state ] ).forEach( event_types => {
            const toStateResult = expected[ from_state ][ event_types ];
            const [toState, actionPerformed] = Array.isArray( toStateResult ) ? toStateResult : [toStateResult,];
            const performed = actionPerformed?` performing ${actionPerformed} `:'';

            it( `go from ${from_state} to ${JSON.stringify(
                toState
            )} on ${event_types}${performed}`, () => {
                const result_state = test_multi_transition( machine, from_state, event_types );

                expect( result_state.value).toBe(toState );
                expect( result_state.context.performed?.action).toBe( actionPerformed );
            } );
        } );
    } );
} );

